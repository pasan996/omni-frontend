import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ObjectGenerator from "./Pages/ObjectGenerator";

function App() {
  return (
    <Router>
      <Switch><Route exact path="/" component={ObjectGenerator}></Route></Switch>
    </Router>
  );
}

export default App;
