import { message } from "antd";
import axios from "axios";

const baseURL = () => {
  const apiUrl = process.env.REACT_APP
    ? process.env.REACT_APP
    : " http://localhost:5000/";
  return apiUrl;
};

const handleResponse = (response) => {
  return {
    data: response && response.data ? response.data.data : null,
    errors: response && response.data ? response.data.errors : {},
    status: response && response.status ? response.status : 500,
    message: response && response.data ? response.data.message : "",
  };
};

const handleError = (errorObject) => {
  console.log(errorObject.data);
  const data = errorObject.response
    ? errorObject.response.data
    : { data: { status: 500, message: errorObject.message } };
  message.error("Error Occured")
};

const OmniApi = {
  createFile: (payload) => {
    let url = `${baseURL()}api/file/create`;
    return axios.post(url, payload).then(handleResponse).catch(handleError);
  },
};

export default OmniApi;
