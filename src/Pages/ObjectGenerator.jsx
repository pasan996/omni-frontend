import { Button, Divider, message, Row, Table } from "antd";
import React, { useState } from "react";
import randomstring from "randomstring";
import OmniApi from "../API/omni.api";

const ObjectGenerator = () => {
  const [data, setData] = useState([]);

  const [dataCount, setDataCount] = useState(null);

  const [link, setLink] = useState(null);

  const generateNumber = async (max, min, count) => {
    message.loading("Generating Data");
    let realNumbers = [];
    let tempInt = [];
    let alphanumerics = [];
    let strings = [];

    for (let i = 0; i < count; i++) {
      realNumbers.push(Math.random() * (max - min) + min);
      tempInt.push(Math.floor(Math.random() * (max - min) + min));
      alphanumerics.push(
        randomstring.generate({
          charset: "alphanumeric",
        })
      );
      strings.push(
        randomstring.generate({
          charset: "alphabetic",
        })
      );
    }
    message.destroy();
    let tempData = [...realNumbers, ...tempInt, ...alphanumerics, ...strings];
    console.log(tempData);
    await setData(tempData);
    message.success("Data Generated");
    // createFile(tempData);
  };

  const generateReport = () => {
    let tempNumbers = {
      integers: 0,
      realNumbers: 0,
      alphanumerics: 0,
      alphabetical: 0,
    };
    let alphabetical = new RegExp(/^[A-Za-z]+$/);
    data.filter((item) => {
      if (typeof item === "number")
        Number.isInteger(item)
          ? tempNumbers.integers++
          : tempNumbers.realNumbers++;
      else if (typeof item === "string") {
        alphabetical.test(item)
          ? tempNumbers.alphabetical++
          : tempNumbers.alphanumerics++;
      }
    });
    console.log(tempNumbers);
    setDataCount(tempNumbers);
  };

  const createFile = async (payload) => {
    message.loading("Creating Download Link");
    try {
      let result = await OmniApi.createFile(payload);
      console.log(result.data);
      message.destroy();
      message.success("Link Created");
      setLink(result.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <React.Fragment>
      <div style={{ padding: "2rem 10rem" }}>
        <Row justify="center">
          <h2>Object Generator</h2>
        </Row>
        <Row justify="space-between">
          <Button
            onClick={() =>
              generateNumber(1000, 1, Math.random() * (15 - 2) + 2)
            }
            type="primary"
          >
            Generate
          </Button>
          {link ? (
            <a
              href={link.location ? link.location : ""}
              target="__blank"
              rel="noopener noreferrer"
              download="file"
            >
              <li>{link.location}</li>
            </a>
          ) : null}
          <Button
            onClick={() => generateReport()}
            disabled={data.length > 0 ? false : true}
          >
            Report
          </Button>
        </Row>
        {data.length > 0 && dataCount ? (
          <Row style={{ marginTop: "1rem" }}>
            <h3>Alphabetical String : {dataCount.alphabetical}</h3>
            <Divider></Divider>
            <h3>Real numbers : {dataCount.realNumbers}</h3>
            <Divider></Divider>
            <h3>Integers : {dataCount.integers}</h3>
            <Divider></Divider>
            <h3>Alphanumerics : {dataCount.alphanumerics}</h3>
            <Divider></Divider>
          </Row>
        ) : null}
      </div>
    </React.Fragment>
  );
};

export default ObjectGenerator;
